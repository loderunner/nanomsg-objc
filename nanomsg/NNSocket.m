//
//  NNSocket.m
//  nanomsg
//
//  Created by Charles Francoise on 15/06/16.
//  Copyright © 2016 Lima Technology, Inc. All rights reserved.
//

#import "NNSocket.h"
#import "NNSocketProtocol.h"

#import "nn.h"
#import "pair.h"
#import "reqrep.h"
#import "pubsub.h"
#import "survey.h"
#import "pipeline.h"
#import "bus.h"

#import <errno.h>

NSString* const NNSocketErrorDomain = @"NNSocketErrorDomain";

static inline int protocolForName(NSString* proto)
{
    if ([proto isEqualToString:NNSocketProtocolPair])
    {
        return NN_PAIR;
    }
    else if ([proto isEqualToString:NNSocketProtocolRequest])
    {
        return NN_REQ;
    }
    else if ([proto isEqualToString:NNSocketProtocolReply])
    {
        return NN_REP;
    }
    else if ([proto isEqualToString:NNSocketProtocolPublish])
    {
        return NN_PUB;
    }
    else if ([proto isEqualToString:NNSocketProtocolSubscribe])
    {
        return NN_SUB;
    }
    else if ([proto isEqualToString:NNSocketProtocolSurveyor])
    {
        return NN_SURVEYOR;
    }
    else if ([proto isEqualToString:NNSocketProtocolRespondent])
    {
        return NN_RESPONDENT;
    }
    else if ([proto isEqualToString:NNSocketProtocolPush])
    {
        return NN_PUSH;
    }
    else if ([proto isEqualToString:NNSocketProtocolPull])
    {
        return NN_PULL;
    }
    else if ([proto isEqualToString:NNSocketProtocolBus])
    {
        return NN_BUS;
    }
    else
    {
        return -1;
    }
}

static inline NSError* NNSocketError(int errnum)
{
    NSString* errorDescription = (errnum > 0)
        ? [NSString stringWithCString:nn_strerror(errnum)
                             encoding:NSUTF8StringEncoding]
        : @"Unknown error.";
    return [NSError errorWithDomain:NNSocketErrorDomain
                               code:errnum
                           userInfo:@{ NSLocalizedDescriptionKey : errorDescription }];
}

@implementation NNSocket
{
    NSMutableDictionary* _localEndpoints;
    NSMutableDictionary* _remoteEndpoints;
}

- (instancetype)initWithProtocol:(NSString *)proto raw:(BOOL)raw
{
    self = [super init];
    if (self != nil)
    {
        _proto = proto;
        _raw = raw;
        _localEndpoints = [NSMutableDictionary dictionary];
        _remoteEndpoints = [NSMutableDictionary dictionary];
        
        int d = raw ? AF_SP_RAW : AF_SP;
        int p = protocolForName(proto);
        
        _sock = nn_socket(d, p);
    }
    return self;
}

- (void)dealloc
{
    [self closeWithError:NULL];
}

- (void)closeWithError:(NSError **)error
{
    int res = nn_close(_sock);
    if ((res < 0) && (error != NULL))
    {
        *error = NNSocketError(errno);
    }
}

- (void)bind:(NSString *)address error:(NSError **)error
{
    int endpoint = nn_bind(_sock, address.UTF8String);
    if (endpoint >= 0)
    {
        _localEndpoints[address] = @(endpoint);
    }
    else if (error != NULL)
    {
        *error = NNSocketError(errno);
    }
}

- (void)connect:(NSString *)address error:(NSError **)error
{
    int endpoint = nn_connect(_sock, address.UTF8String);
    if (endpoint >= 0)
    {
        _remoteEndpoints[address] = @(endpoint);
    }
    else if (error != NULL)
    {
        *error = NNSocketError(errno);
    }
}

- (void)shutdown:(NSString *)address error:(NSError **)error
{
    int res = EINVAL;
    
    NSNumber* localEndpoint = _localEndpoints[address];
    if (localEndpoint != nil)
    {
        res = nn_shutdown(_sock, localEndpoint.intValue);
        
        if ((res < 0) && (errno != EINVAL))
        {
            if (error != NULL)
            {
                *error = NNSocketError(errno);
            }
            
            // Can't recover from error - bail early
            return;
        }
    }
    
    NSNumber* remoteEndpoint = _remoteEndpoints[address];
    if (remoteEndpoint != nil)
    {
        res = nn_shutdown(_sock, remoteEndpoint.intValue);
    }
    
    if ((res < 0) && (*error != NULL))
    {
        *error = NNSocketError(errno);
    }
}

- (void)sendMessage:(NSData *)message wait:(BOOL)wait error:(NSError **)error
{
    int res = nn_send(_sock, message.bytes, message.length, wait ? 0 : NN_DONTWAIT);
    
    if ((res < 0) && (error != NULL))
    {
        *error = NNSocketError(errno);
    }
}

- (NSData *)receiveMessageWithWait:(BOOL)wait error:(NSError **)error
{
    char *msg;
    int res = nn_recv(_sock, &msg, NN_MSG, wait ? 0 : NN_DONTWAIT);
    
    if (res >= 0)
    {
        NSData* message = [NSData dataWithBytes:msg length:res];
        nn_freemsg(msg);
        return message;
    }
    else
    {
        if (error != NULL)
        {
            *error = NNSocketError(errno);
        }
        
        return nil;
    }
}

#pragma mark -
#pragma mark Properties
@dynamic localEndpoints;
@dynamic remoteEndpoints;
@dynamic linger;
@dynamic sendBufferSize;
@dynamic receiveBufferSize;
@dynamic maximumReceiveMessageSize;
@dynamic sendTimeout;
@dynamic receiveTimeout;
@dynamic reconnectInterval;
@dynamic maximumReconnectInterval;
@dynamic sendPriority;
@dynamic receivePriority;
@dynamic ipv4Only;
@dynamic name;
@dynamic ttl;

- (NSArray<NSString *> *)localEndpoints
{
    return _localEndpoints.allKeys;
}

- (NSArray<NSString *> *)remoteEndpoints
{
    return _remoteEndpoints.allKeys;
}

- (int)linger
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_LINGER, &res, &sz);
    
    return res;
}

- (void)setLinger:(int)linger
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_LINGER, &linger, sizeof(linger));
}

- (int)sendBufferSize
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_SNDBUF, &res, &sz);
    
    return res;
}

- (void)setSendBufferSize:(int)sendBufferSize
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_SNDBUF, &sendBufferSize, sizeof(sendBufferSize));
}

- (int)receiveBufferSize
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_RCVBUF, &res, &sz);
    
    return res;
}

- (void)setReceiveBufferSize:(int)receiveBufferSize
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_RCVBUF, &receiveBufferSize, sizeof(receiveBufferSize));
}

- (int)maximumReceiveMessageSize
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_RCVMAXSIZE, &res, &sz);
    
    return res;
}

- (void)setMaximumReceiveMessageSize:(int)maximumReceiveMessageSize
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_RCVMAXSIZE, &maximumReceiveMessageSize, sizeof(maximumReceiveMessageSize));
}

- (int)sendTimeout
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_SNDTIMEO, &res, &sz);
    
    return res;
}

- (void)setSendTimeout:(int)sendTimeout
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_SNDTIMEO, &sendTimeout, sizeof(sendTimeout));
}

- (int)receiveTimeout
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_RCVTIMEO, &res, &sz);
    
    return res;
}

- (void)setReceiveTimeout:(int)receiveTimeout
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_RCVTIMEO, &receiveTimeout, sizeof(receiveTimeout));
}

- (int)reconnectInterval
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_RECONNECT_IVL, &res, &sz);
    
    return res;
}

- (void)setReconnectInterval:(int)reconnectInterval
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_RECONNECT_IVL, &reconnectInterval, sizeof(reconnectInterval));
}

- (int)maximumReconnectInterval
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_RECONNECT_IVL_MAX, &res, &sz);
    
    return res;
}

- (void)setMaximumReconnectInterval:(int)maximumReconnectInterval
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_RECONNECT_IVL_MAX, &maximumReconnectInterval, sizeof(maximumReconnectInterval));
}

- (int)sendPriority
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_SNDPRIO, &res, &sz);
    
    return res;
}

- (void)setSendPriority:(int)sendPriority
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_SNDPRIO, &sendPriority, sizeof(sendPriority));
}

- (int)receivePriority
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_RCVPRIO, &res, &sz);
    
    return res;
}

- (void)setReceivePriority:(int)receivePriority
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_RCVPRIO, &receivePriority, sizeof(receivePriority));
}

- (BOOL)isIpv4Only
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_IPV4ONLY, &res, &sz);
    
    return res;
}

- (void)setIpv4Only:(BOOL)ipv4Only
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_IPV4ONLY, &ipv4Only, sizeof(ipv4Only));
}

- (NSString *)name
{
    char str[256];
    size_t sz = 256;
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_SOCKET_NAME, &str, &sz);
    
    return @(str);
}

- (void)setName:(NSString *)name
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_SOCKET_NAME, name.UTF8String, sizeof([name lengthOfBytesUsingEncoding:NSUTF8StringEncoding]));
}

- (int)ttl
{
    int res;
    size_t sz = sizeof(res);
    nn_getsockopt(_sock, NN_SOL_SOCKET, NN_MAXTTL, &res, &sz);
    
    return res;
}

- (void)setTtl:(int)ttl
{
    nn_setsockopt(_sock, NN_SOL_SOCKET, NN_MAXTTL, &ttl, sizeof(ttl));
}

@end

