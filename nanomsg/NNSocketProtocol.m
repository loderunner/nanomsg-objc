//
//  NNSocketProtocol.m
//  nanomsg
//
//  Created by Charles Francoise on 16/06/16.
//  Copyright © 2016 Lima Technology, Inc. All rights reserved.
//

#import "NNSocketProtocol.h"

NSString* const NNSocketProtocolPair = @"NNSocketProtocolPair";
NSString* const NNSocketProtocolRequest = @"NNSocketProtocolRequest";
NSString* const NNSocketProtocolReply = @"NNSocketProtocolReply";
NSString* const NNSocketProtocolPublish = @"NNSocketProtocolPublish";
NSString* const NNSocketProtocolSubscribe = @"NNSocketProtocolSubscribe";
NSString* const NNSocketProtocolSurveyor = @"NNSocketProtocolSurveyor";
NSString* const NNSocketProtocolRespondent = @"NNSocketProtocolRespondent";
NSString* const NNSocketProtocolPush = @"NNSocketProtocolPush";
NSString* const NNSocketProtocolPull = @"NNSocketProtocolPull";
NSString* const NNSocketProtocolBus = @"NNSocketProtocolBus";