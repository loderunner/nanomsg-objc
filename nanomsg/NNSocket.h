//
//  NNSocket.h
//  nanomsg
//
//  Created by Charles Francoise on 15/06/16.
//  Copyright © 2016 Lima Technology, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString* const NNSocketErrorDomain;


@interface NNSocket : NSObject

// The underlying socket descriptor (read-only). Note that nanomsg socket descriptors are not standard file descriptors and will exhibit undefined behaviour when used with system functions. Moreover, it may happen that a system file descriptor and nanomsg socket descriptor will incidentally collide (be equal). A negative number designates an invalid socket.
@property (nonatomic, readonly) int sock;

// A list of endpoints locally bound to this socket using `bind:error:`.
@property (nonatomic, readonly) NSArray<NSString*>* localEndpoints;

// A list of endpoints this socket has connected to using `connect:error:`.
@property (nonatomic, readonly) NSArray<NSString*>* remoteEndpoints;

// A boolean value that indicates whether the socket is "raw". Raw sockets omit the end-to-end functionality found in standard sockets and thus can be used to implement intermediary devices in topologies.
@property (nonatomic, readonly) BOOL raw;

// The nanomsg protocol used by this socket.
@property (nonatomic, readonly) NSString* proto;

// Specifies how long the socket should try to send pending outbound messages after `close` has been called, in milliseconds. Negative value means infinite linger. Default value is 1000 (1 second).
@property (nonatomic, assign) int linger;

// Size of the send buffer, in bytes. To prevent blocking for messages larger than the buffer, exactly one message may be buffered in addition to the data in the send buffer. Default value is 128kB.
@property (nonatomic, assign) int sendBufferSize;

// Size of the receive buffer, in bytes. To prevent blocking for messages larger than the buffer, exactly one message may be buffered in addition to the data in the receive buffer. Default value is 128kB.
@property (nonatomic, assign) int receiveBufferSize;

// Maximum message size that can be received, in bytes. Negative value means that the received size is limited only by available addressable memory. Default is 1024kB.
@property (nonatomic, assign) int maximumReceiveMessageSize;

// The timeout for send operation on the socket, in milliseconds. Negative value means infinite timeout. Default value is -1.
@property (nonatomic, assign) int sendTimeout;

// The timeout for receive operation on the socket, in milliseconds. Negative value means infinite timeout. Default value is -1.
@property (nonatomic, assign) int receiveTimeout;

// How long to wait, in milliseconds, when connection is broken before trying to re-establish it. Used in connection-based transports such as TCP. Note that actual reconnect interval may be randomised to some extent to prevent severe reconnection storms. Default value is 100 (0.1 second).
@property (nonatomic, assign) int reconnectInterval;

// Maximum reconnection interval, to be used only in addition to `reconnectInterval`. On each reconnect attempt, the previous interval is doubled until maximum is reached. Value of zero means that no exponential backoff is performed and reconnect interval is constant. If `maximumReconnectInterval` is less than `reconnectInterval`, it is ignored. Default value is 0.
@property (nonatomic, assign) int maximumReconnectInterval;

// Outbound priority currently set on the socket. This option has no effect on socket types that send messages to all the peers. However, if the socket type sends each message to a single peer (or a limited set of peers), peers with high priority take precedence over peers with low priority. Highest priority is 1, lowest priority is 16. Default value is 8.
@property (nonatomic, assign) int sendPriority;

// Sets inbound priority for endpoints subsequently added to the socket. This option has no effect on socket types that are not able to receive messages. When receiving a message, messages from peer with higher priority are received before messages from peer with lower priority. Highest priority is 1, lowest priority is 16. Default value is 8.
@property (nonatomic, assign) int receivePriority;

// When true, only IPv4 addresses are used. Otherwise, both IPv4 and IPv6 addresses are used. Default value is `YES`.
@property (nonatomic, assign) BOOL ipv4Only;

// Socket name for error reporting and statistics. The type of the option is string. Default value is "socket.N" where N is socket integer.
@property (nonatomic, assign) NSString* name;

// Sets the maximum number of "hops" a message can go through before it is dropped. Each time the message is received (for example via the `nn_device` function) counts as a single hop. This provides a form of protection against inadvertent loops.
@property (nonatomic, assign) int ttl;

// Creates a new socket. The newly created socket is initially not associated with any endpoints. In order to establish a message flow at least one endpoint has to be added to the socket using `bind:` or `connect:` function.
- (instancetype)initWithProtocol:(NSString*)proto raw:(BOOL)raw;

// Closes the socket. Any buffered inbound messages that were not yet received by the application will be discarded. The library will try to deliver any outstanding outbound messages for the time specified by `linger` property. The call will block in the meantime.
- (void)closeWithError:(NSError**)error;

// Adds a local endpoint to the socket. The endpoint can be then used by other applications to connect to. The `address` argument consists of two parts as follows: `transport://address`. The transport specifies the underlying transport protocol to use. The meaning of the address part is specific to the underlying transport protocol.
- (void)bind:(NSString*)address error:(NSError**)error;

// Adds a remote endpoint to the socket. The library will then try to connect to the specified remote endpoint. The `address` argument consists of two parts as follows: `transport://address`. The transport specifies the underlying transport protocol to use. The meaning of the address part is specific to the underlying transport protocol.
- (void)connect:(NSString*)address error:(NSError**)error;

// Removes an endpoint from socket. `address` parameter specifies the ID of the endpoint to remove as returned by prior call to bind: or connect:. `shutdown:` call will return immediately, however, the library will try to deliver any outstanding outbound messages to the endpoint for the time specified by the `linger` property.
- (void)shutdown:(NSString*)address error:(NSError**)error;

// Send a message containing the data through a socket. Which of the peers the message will be sent to is determined by the particular socket type. `wait` argument specifies that the operation should be performed in non-blocking mode. If the message cannot be sent straight away, the function will fail
- (void)sendMessage:(NSData*)message wait:(BOOL)wait error:(NSError**)error;

// Receive a message from the socket and return it.
- (NSData*)receiveMessageWithWait:(BOOL)wait error:(NSError**)error;

@end



