//
//  NNSocketProtocol.h
//  nanomsg
//
//  Created by Charles Francoise on 16/06/16.
//  Copyright © 2016 Lima Technology, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

// Pair protocol - one-to-one protocol

// Socket for communication with exactly one peer. Each party can send messages at any time. If the peer is not available or send buffer is full subsequent calls to `send:error:` will block until it’s possible to send the message.
extern NSString* const NNSocketProtocolPair;

// Request/reply protocol - distribute the workload among multiple stateless workers

// Socket for client application that sends requests and receives replies.
extern NSString* const NNSocketProtocolRequest;
// Socket for stateless worker that receives requests and sends replies
extern NSString* const NNSocketProtocolReply;

// Publish/Subscribe protocol - broadcasts messages to multiple destinations.

// Socket to distribute messages to multiple destinations. Receive operation is not defined.
extern NSString* const NNSocketProtocolPublish;
// Socket to receive messages from the publisher. Only messages that the socket is subscribed to are received. When the socket is created there are no subscriptions and thus no messages will be received. Send operation is not defined on this socket.
extern NSString* const NNSocketProtocolSubscribe;

// Survey protocol - allows to broadcast a survey to multiple locations and gather the responses.

// Socket used to send the survey. The survey is delivered to all the connected respondents. Once the query is sent, the socket can be used to receive the responses. When the survey deadline expires, receive will return ETIMEDOUT error.
extern NSString* const NNSocketProtocolSurveyor;
// Socket used to respond to the survey. Survey is received using receive function, response is sent using send function. This socket can be connected to at most one peer.
extern NSString* const NNSocketProtocolRespondent;

// Pipeline protocol - fair queues messages from the previous processing step and load balances them among instances of the next processing step.

// Socket used to send messages to a cluster of load-balanced nodes. Receive operation is not implemented on this socket type.
extern NSString* const NNSocketProtocolPush;
// Socket is used to receive a message from a cluster of nodes. Send operation is not implemented on this socket type.
extern NSString* const NNSocketProtocolPull;

// Bus protocol - broadcasts messages from any node to all other nodes in the topology. The socket should never receive messages that it sent itself.

// Socket sent messages are distributed to all nodes in the topology. Incoming messages from all other nodes in the topology are fair-queued in the socket.
extern NSString* const NNSocketProtocolBus;